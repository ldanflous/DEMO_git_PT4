DEMO Git - Gitlab runner - SonarQube
==============================
[![pipeline status](https://gitlab-ce.iut.u-bordeaux.fr/PT4/DEMO-GIT-PT4/badges/master/pipeline.svg)](https://gitlab-ce.iut.u-bordeaux.fr/PT4/DEMO-GIT-PT4/pipelines) [![coverage report](https://gitlab-ce.iut.u-bordeaux.fr/PT4/DEMO-GIT-PT4/badges/master/coverage.svg)](http://alee.iut.bx1:9000/dashboard?id=my%3Achess)


Get this project
----------

To use last development state of this project, please clone the master
branch. Note that if the project contains some submodules, please use these commands:

    # if git version >= 1.9
      git clone --recursive git@gitlab-ce.iut.u-bordeaux.fr:PT4/DEMO-GIT-PT4.git
      cd DEMO-GIT-PT4 
    # else
      git clone git@gitlab-ce.iut.u-bordeaux.fr:PT4/DEMO-GIT-PT4.git
      cd DEMO-GIT-PT4
      git submodule init
      git submodule update

Documentation
-------------

Installation
------------

### Build and install with CMake

This project can be built using [CMake](https://cmake.org/). 
CI is enable with Docker image [tthor/test](https://hub.docker.com/r/tthor/test/).
Reports will be available at [SonarQube](http://alee.iut.bx1:9000/dashboard?id=my%3Achess)

Get involved!
---------------------

### Reporting an issue

We strongly recommend all users to use the issue tracker to report any problems with the software, or for any feature request. We will try our best to answer them in a short time frame.

### Contributions

https://gitlab-ce.iut.u-bordeaux.fr/PT4/DEMO-GIT-PT4/blob/master/CONTRIBUTING.md

### Authors

The following people contribute or contributed to the development of this project:
  * Pierre Ramet, PI

If we forgot your name, please let us know that we can fix that mistake.

coucou